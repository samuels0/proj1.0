package enigma;

import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.Timeout;
import static org.junit.Assert.*;

import static enigma.TestUtils.*;

/** The suite of all JUnit tests for the Permutation class.
 *  @author
 */
public class PermutationTest {

    /** Testing time limit. */
    @Rule
    public Timeout globalTimeout = Timeout.seconds(500);

    /* ***** TESTING UTILITIES ***** */

    private Permutation perm;
    private String alpha = UPPER_STRING;

    /** Check that perm has an alphabet whose size is that of
     *  FROMALPHA and TOALPHA and that maps each character of
     *  FROMALPHA to the corresponding character of FROMALPHA, and
     *  vice-versa. TESTID is used in error messages. */
    private void checkPerm(String testId,
                           String fromAlpha, String toAlpha) {
        int N = fromAlpha.length();
        assertEquals(testId + " (wrong length)", N, perm.size());
        for (int i = 0; i < N; i += 1) {
            char c = fromAlpha.charAt(i), e = toAlpha.charAt(i);
            char n = perm.permute(c);
            assertEquals(msg(testId, "wrong translation of '%c'", c),
                         e, perm.permute(c));
            assertEquals(msg(testId, "wrong inverse of '%c'", e),
                         c, perm.invert(e));
            int ci = alpha.indexOf(c), ei = alpha.indexOf(e);
            assertEquals(msg(testId, "wrong translation of %d", ci),
                         ei, perm.permute(ci));
            assertEquals(msg(testId, "wrong inverse of %d", ei),
                         ci, perm.invert(ei));
        }
    }

    /* ***** TESTS ***** */

    @Test
    public void checkIdTransform() {
        perm = new Permutation("", UPPER);
        checkPerm("identity", UPPER_STRING, UPPER_STRING);
    }
    @Test
    public void checkABTransform() {
        perm = new Permutation("(AB)", UPPER);
        checkPerm("AB swap",UPPER_STRING, "BACDEFGHIJKLMNOPQRSTUVWXYZ");
    }
    @Test
    public void checkABCTransform() {
        perm = new Permutation("(ABC)", UPPER);
        checkPerm("ABC cycle",UPPER_STRING, "BCADEFGHIJKLMNOPQRSTUVWXYZ");
    }

    @Test
    public void checkABCABCTransform() {
        perm = new Permutation("(ABC)(ABC)", UPPER);
        checkPerm("ABC then ABC cycle",UPPER_STRING, "CABDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    @Test
    public void checkManyTransform() {
        perm = new Permutation("(AB)(CD)(EF)(AB)(EF)", UPPER);
        checkPerm("many 2-cycles",UPPER_STRING, "ABDCEFGHIJKLMNOPQRSTUVWXYZ");
    }


    @Test
    public void checkFullLengthTransform() {
        perm = new Permutation("(ABCDEFGHIJKLMNOPQRSTUVWXYZ)", UPPER);
        checkPerm("Full length cycle",UPPER_STRING, "BCDEFGHIJKLMNOPQRSTUVWXYZA");
    }
    @Test
    public void checkMTransform() {
        perm = new Permutation("(HQ) (EX) (IP) (TR) (BY)", UPPER);
        checkPerm("Full length cycle",UPPER_STRING, "AYCDXFGQPJKLMNOIHTSRUVWEBZ");
    }





    @Test
    public void checkABABTransform() {
        perm = new Permutation("(AB)(AB)", UPPER);
        checkPerm("AB AB cycle",UPPER_STRING, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    @Test
    public void checkABABABTransform() {
        perm = new Permutation("(AB)(AB) (AB)", UPPER);
        checkPerm("AB AB AB cycle",UPPER_STRING, "BACDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    @Test
    public void checkOverlapTransform() {
        perm = new Permutation("(AB) (BC)", UPPER);
        checkPerm("AB BC cycles",UPPER_STRING, "BCADEFGHIJKLMNOPQRSTUVWXYZ");
    }





    @Test
    public void checkInverse() {
        perm = new Permutation("(ABC)", UPPER);
        for (int i = 0; i < 3; i++ );
        int x = perm.invert(0);
        int y = perm.invert(1);
        int z = perm.invert(2);
    }

}
