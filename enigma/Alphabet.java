package enigma;

import java.util.HashMap;

/** An alphabet of encodable characters.  Provides a mapping from characters
 *  to and from indices into the alphabet.
 *  @author
 */
class Alphabet {

    /** A new alphabet containing CHARS. The K-th character has index
     *  K (numbering from 0). No character may be duplicated. */
    String characters;

    Alphabet(String chars) {
         this.characters = chars;
    }

    /** A default alphabet of all upper-case characters. */
    Alphabet() {
        this("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    }

    /** Returns the size of the alphabet. */
    int size() {
        return characters.length();
    }

    /** Returns true if CH is in this alphabet. */
    boolean contains(char ch) {
        return this.characters.contains(String.valueOf(ch)); // FIXME x
    }

    /** Returns character number INDEX in the alphabet, where
     *  0 <= INDEX < size(). */
    char toChar(int index) {
        if ((index < 0) || (index >= characters.length())) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return characters.charAt(index);
    }

    /** Returns the index of character CH which must be in
     *  the alphabet. This is the inverse of toChar(). */
    int toInt(char ch) {
        return characters.indexOf(String.valueOf(ch));
    }

    /** Returns the alphabet as a hashmap with key = char, value = [cur index, prev index, original index] This is a new method.
     */
    public HashMap<Character, int[]> toHashMap() {
        HashMap<Character, int[]> alphabetMap = new HashMap<>();
        for (int i = 0; i < this.characters.length(); i++) {
            alphabetMap.put(characters.charAt(i), new int[]{i,i,i});
        }
        return alphabetMap;
    }

}
