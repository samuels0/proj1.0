package enigma;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;

import static enigma.EnigmaException.*;

/** Class that represents a complete enigma machine.
 *  @author
 */
class Machine {

    /** A new Enigma machine with alphabet ALPHA, 1 < NUMROTORS rotor slots,
     *  and 0 <= PAWLS < NUMROTORS pawls.  ALLROTORS contains all the
     *  available rotors. */
    Machine(Alphabet alpha, int numRotors, int pawls,
            Collection<Rotor> allRotors) {
        _alphabet = alpha;
        _numRotors = numRotors;
        _pawls = pawls;
        _allRotors = new ArrayList<>();
        _allRotors.addAll(allRotors);
        _rotorArrayList = new ArrayList<>();

        // FIXME
    }

    /** Return the number of rotor slots I have. */
    int numRotors() {
        return _numRotors;
    }

    /** Return the number pawls (and thus rotating rotors) I have. */
    int numPawls() {
        return _pawls;
    }

    /** Return Rotor #K, where Rotor #0 is the reflector, and Rotor
     *  #(numRotors()-1) is the fast Rotor.  Modifying this Rotor has
     *  undefined results. */
    Rotor getRotor(int k) {
        return _rotorArrayList.get(k);
    }

    Alphabet alphabet() {
        return _alphabet;
    }

    /** Set my rotor slots to the rotors named ROTORS from my set of
     *  available rotors (ROTORS[0] names the reflector).
     *  Initially, all rotors are set at their 0 setting. */
    void insertRotors(String[] rotors) {
        _rotorArrayList = new ArrayList<>();
        for (int i = 0; i < rotors.length; i++) {
            for (int j = 0; j < _allRotors.size(); j++) {
                String allRotorName = _allRotors.get(j).name();
                String rotorName = rotors[i];
                if (_allRotors.get(j).name().equals(rotorName)) {
                    if (!_rotorArrayList.contains(_allRotors.get(j))) {
                        _rotorArrayList.add(_allRotors.get(j));
                    } else {
                        throw new EnigmaException("Rotor can only be placed once");
                    }
                }
            }
        }
    }

    /** Set my rotors according to SETTING, which must be a string of
     *  numRotors()-1 characters in my alphabet. The first letter refers
     *  to the leftmost rotor setting (not counting the reflector).
     *  check for reflector?
     *  */
    void setRotors(String setting) {
        if (setting.length() != (numRotors() - 1)) {
            throw new EnigmaException("Invalid setting");
        }
        if ( _rotorArrayList.get(0).reflecting() && setting.length() + 1 == _rotorArrayList.size()) {
            for (int i = 0; i < _rotorArrayList.size() - 1; i++) {
                if (i + 1 < numRotors() - _pawls) {
                    if ((_rotorArrayList.get(i + 1).reflecting()
                            || _rotorArrayList.get(i + 1).rotates())) {
                        throw new EnigmaException("Invalid setting");
                    }
                    else {
                        _rotorArrayList.get(i + 1).set(_alphabet.toInt(setting.charAt(i)));
                    }
                } else if (_rotorArrayList.size() - _pawls <= i + 1){
                    if (!(_rotorArrayList.get(i + 1).rotates())) {
                        throw new EnigmaException("Invalid setting");

                    } else {
                        _rotorArrayList.get(i + 1).set(_alphabet.toInt((setting.charAt(i))));
                    }
                }
            }
        }
        else {
            throw new EnigmaException("Invalid settting");
        }
    }
    /** Return the current plugboard's permutation. */
    Permutation plugboard() {
        return _plugboard;
    }

    /** Set the plugboard to PLUGBOARD. */
    void setPlugboard(Permutation plugboard) {
        _plugboard = plugboard;
    }

    /** Returns the result of converting the input character C (as an
     *  index in the range 0..alphabet size - 1), after first advancing
     *  the machine. */
    int convert(int c) {
        advanceRotors();
        if (Main.verbose()) {
            System.err.printf("[");
            for (int r = 1; r < numRotors(); r += 1) {
                System.err.printf("%c",
                        alphabet().toChar(getRotor(r).setting()));
            }
            System.err.printf("] %c -> ", alphabet().toChar(c));
        }
        c = plugboard().permute(c);
        if (Main.verbose()) {
            System.err.printf("%c -> ", alphabet().toChar(c));
        }
        c = applyRotors(c);
        c = plugboard().permute(c);
        if (Main.verbose()) {
            System.err.printf("%c%n", alphabet().toChar(c));
        }
        return c;
    }

    /** Advance all rotors to their next position. */
    private void advanceRotors() {
        ArrayList<Rotor> rotorsToAdvance = new ArrayList<>();
        for (int i = 0; i < _pawls; i++) {
            int j = i + numRotors() - _pawls;

            if (j + 1  == numRotors()) {
                rotorsToAdvance.add(_rotorArrayList.get(j));

            } else if (_rotorArrayList.get(j + 1).atNotch()
                    || rotorsToAdvance.contains(_rotorArrayList.get(j - 1))) {
                if (!rotorsToAdvance.contains(_rotorArrayList)) {
                    rotorsToAdvance.add(_rotorArrayList.get(j));
                }
                if (_rotorArrayList.get(j).atNotch()) {
                    if (!rotorsToAdvance.contains(_rotorArrayList.get(j - 1))) {
                        rotorsToAdvance.add(_rotorArrayList.get(j - 1));
                    }
                }
            }
        }

        for (int i = 0; i < rotorsToAdvance.size(); i++) {
            rotorsToAdvance.get(i).advance();
        }

    }

    /** Return the result of applying the rotors to the character C (as an
     *  index in the range 0..alphabet size - 1). Go through rotors, in then out. */
    private int applyRotors(int c) {
        int cImage = c;
        for (int i = 0; i < _numRotors; i++) {
            cImage = _rotorArrayList.get(_numRotors - 1 - i).convertForward(cImage);
        }
        for (int i = 0; i < _numRotors - 1; i++) {
            cImage = _rotorArrayList.get(i + 1).convertBackward(cImage);
        }
        return cImage;
    }

    /** Returns the encoding/decoding of MSG, updating the state of
     *  the rotors accordingly. */
    String convert(String msg) {
        msg = msg.replaceAll(" ", "");
        String message = "";
        for (int i = 0; i < msg.length(); i++) {
            char cTest = msg.charAt(i);
            int iTest = _alphabet.toInt(msg.charAt(i));
            int conTest = convert(_alphabet.toInt(msg.charAt(i)));
            char charTest = _alphabet.toChar(conTest);
            message = message + charTest;
        }
        return message;
    }

    /** Common alphabet of my rotors. */
    private final Alphabet _alphabet;
    private int _pawls;
    private Permutation _plugboard;
    private ArrayList<Rotor> _rotorArrayList;
    private ArrayList<Rotor> _allRotors;
    private int _numRotors;
}
